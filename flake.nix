{
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = with pkgs; mkShell {
          packages = [
            clang-tools_16
            gdb
            binutils
          ];

          buildInputs = [
            gcc
            cmake
            ninja
          ];

          INPUTS = toString ./inputs;

          CMAKE_EXPORT_COMPILE_COMMANDS = 1;
        };
      }
    );
}
