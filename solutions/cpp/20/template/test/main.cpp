#include "solution.hpp"
#include <sstream>

void test(const std::string input, const std::string expected_output) {
  std::stringstream in(input);
  std::stringstream out;
  solve(in, out);
  std::string output(out.str());
  if (expected_output != output) {
    std::cerr << "Expected: " << expected_output << "\nGot: " << output
              << std::endl;
    exit(1);
  }
}

int main() {
  test("", "");
  return 0;
}
