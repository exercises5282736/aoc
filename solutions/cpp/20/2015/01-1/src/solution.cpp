#include "solution.hpp"
#include <algorithm>
#include <iterator>
#include <numeric>

int solve(std::istream &in, std::ostream &out) {
  std::istreambuf_iterator<char> start(in), end;
  int floor = 0;
  int position = 0;

  for (auto iter = start; iter != end && floor >= 0; iter++, position++) {
    switch (*iter) {
      case '(':
        floor += 1;
        break;
      case ')':
        floor -= 1;
        break;
      default:
        break;
    }
  }
  
  out << position;
  return 0;
}
