#include "solution.hpp"
#include <algorithm>
#include <iterator>
#include <numeric>

int solve(std::istream &in, std::ostream &out) {
  std::istreambuf_iterator<char> start(in), end;
  int floor = std::accumulate(start, end, 0, [](int floor, char c) {
    switch (c) {
    case '(':
      return floor + 1;
    case ')':
      return floor - 1;
    default:
      return floor;
    }
  });
  out << floor;
  return 0;
}
