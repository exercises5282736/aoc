#include "solution.hpp"
#include <iostream>

int main() {
  solve(std::cin, std::cout);
  std::cout << std::endl;
  return 0;
}
