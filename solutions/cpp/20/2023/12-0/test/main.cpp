#include "solution.hpp"
#include <sstream>

void test(const std::string input, const std::string expected_output) {
  std::stringstream in(input);
  std::stringstream out;
  solve(in, out);
  std::string output(out.str());
  if (expected_output != output) {
    std::cerr << "Expected: " << expected_output << "\nGot: " << output
              << std::endl;
    exit(1);
  }
}

int main() {
  test("???.### 1,1,3\n", "1");
  test(".??..??...?##. 1,1,3\n", "4");
  test("?#?#?#?#?#?#?#? 1,3,1,6\n", "1");
  test("????.#...#... 4,1,1\n", "1");
  test("????.######..#####. 1,6,5\n", "4");
  test("?###???????? 3,2,1\n", "10");
  return 0;
}
 // split .
