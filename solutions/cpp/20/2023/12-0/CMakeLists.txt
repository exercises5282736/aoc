cmake_minimum_required(VERSION 3.26)

project(aoc_cpp_20_202312-0)

set(CMAKE_CXX_STANDARD 20)

include(CTest)

include_directories(src)

add_library(sol202312-0 STATIC src/solution.cpp)

add_executable(202312-0 src/main.cpp)
target_link_libraries(202312-0 sol202312-0)

add_executable(202312-0_test test/main.cpp)
target_link_libraries(202312-0_test sol202312-0)
add_test(NAME 202312-0_test COMMAND 202312-0_test)
