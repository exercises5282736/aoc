#ifndef _SOLUTION_HPP
#define _SOLUTION_HPP

#include <iostream>

int solve(std::istream &in, std::ostream &out);

#endif // _SOLUTION_HPP
