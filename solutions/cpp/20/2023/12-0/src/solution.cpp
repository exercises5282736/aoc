#include "solution.hpp"
#include <string>
#include <ranges>
#include <charconv>

int compute_solutions(std::string& pattern, size_t groups_len, char *groups);

int solve(std::istream &in, std::ostream &out) {
  int sum = 0;
  char groups[16];

  while (in) {
    size_t groups_len = 0;
    std::string pattern, raw_groups;
    std::getline(in, pattern, ' ');
    std::getline(in, raw_groups, '\n');
    for (const auto group : std::views::split(raw_groups, ',')) {
      const auto sv = std::string_view(group.begin(), group.end());
      std::from_chars(sv.data(), sv.data() + sv.size(), groups[groups_len++]);
    }
    sum += compute_solutions(pattern, groups_len, groups);
  }
  
  out << sum;
  return 0;
}

int compute_solutions(std::string& pattern, size_t groups_len, char *groups)
{
  // reduce .. to .
  // split by .
  // recursive search
  return 0;
}
