#include "solution.hpp"
#include <iterator>

constexpr bool is_digit(const char c) { return c >= '0' && c <= '9'; }

int solve(std::istream &in, std::ostream &out) {
  std::istreambuf_iterator<char> start(in), end;
  char first = 0, last = 0;
  int sum = 0;

  for (auto iter = start; iter != end; iter++) {
    const auto c = *iter;
    if (is_digit(c)) {
      if (first == 0) {
        first = c - '0';
        last = first;
      } else {
        last = c - '0';
      }
    }
    if (c == '\n') {
      sum += first * 10 + last;
      first = 0;
      last = 0;
    }
  }

  out << sum;
  return 0;
}
