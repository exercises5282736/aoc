#include "solution.hpp"
#include <cstdint>
#include <iterator>

const size_t NO_STATES = 50;

void define_state_machine(uint8_t sm[NO_STATES][256]) {
  for (int i = 0; i < NO_STATES; i++) { // define global edges
    sm[i]['1'] = 41; // unfortunatly we cannot reuse states 1-9
    sm[i]['2'] = 42; // because 2ne for example would be interpreted
    sm[i]['3'] = 43; // as twone (21)
    sm[i]['4'] = 44;
    sm[i]['5'] = 45;
    sm[i]['6'] = 46;
    sm[i]['7'] = 47;
    sm[i]['8'] = 48;
    sm[i]['9'] = 49;
    sm[i]['o'] = 11;
    sm[i]['t'] = 17;
    sm[i]['f'] = 22;
    sm[i]['s'] = 27;
    sm[i]['e'] = 21;
    sm[i]['n'] = 32;
  }
  sm[1]['i'] = 13;
  sm[2]['n'] = 12;
  sm[3]['i'] = 13;
  sm[5]['i'] = 21;
  sm[7]['i'] = 33;
  sm[8]['w'] = 16;
  sm[9]['i'] = 13;
  sm[11]['n'] = 12;
  sm[12]['e'] = 1;
  sm[12]['i'] = 33;
  sm[13]['g'] = 14;
  sm[14]['h'] = 15;
  sm[15]['t'] = 8;
  sm[16]['o'] = 2;
  sm[17]['w'] = 16;
  sm[17]['h'] = 18;
  sm[18]['r'] = 19;
  sm[19]['e'] = 20;
  sm[20]['e'] = 3;
  sm[20]['i'] = 13;
  sm[21]['i'] = 13;
  sm[22]['o'] = 23;
  sm[22]['i'] = 25;
  sm[23]['n'] = 12;
  sm[23]['u'] = 24;
  sm[24]['r'] = 4;
  sm[25]['v'] = 26;
  sm[26]['e'] = 5;
  sm[27]['i'] = 28;
  sm[27]['e'] = 29;
  sm[28]['x'] = 6;
  sm[29]['i'] = 13;
  sm[29]['v'] = 30;
  sm[30]['e'] = 31;
  sm[31]['n'] = 7;
  sm[31]['i'] = 13;
  sm[32]['i'] = 33;
  sm[33]['n'] = 34;
  sm[34]['i'] = 33;
  sm[34]['e'] = 9;
}

int solve(std::istream &in, std::ostream &out) {
  uint8_t state_machine[NO_STATES][256] = {};
  define_state_machine(state_machine);

  std::istreambuf_iterator<char> start(in), end;
  uint8_t state = 0;
  char first = 0, last = 0;
  int sum = 0;

  for (auto iter = start; iter != end; iter++) {
    const auto c = *iter;
    state = state_machine[state][c];
    int digit = 0;
    if (state >= 1 && state <= 9)
      digit = state;
    if (state >= 40 && state <= 49)
      digit = state - 40;
    if (digit != 0) {
      if (first == 0) {
        first = digit;
        last = first;
      } else {
        last = digit;
      }
    }
    if (c == '\n') {
      sum += first * 10 + last;
      first = 0;
      last = 0;
    }
  }

  out << sum;
  return 0;
}
