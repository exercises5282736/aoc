#include "solution.hpp"
#include <sstream>

void test(const std::string input, const std::string expected_output) {
  std::stringstream in(input);
  std::stringstream out;
  solve(in, out);
  std::string output(out.str());
  if (expected_output != output) {
    std::cerr << "Expected: " << expected_output << "\nGot: " << output
              << std::endl;
    exit(1);
  }
}

int main() {
  test("two1nine\neightwothree\nabcone2threexyz\nxtwone3four\n4nineeightseven2"
       "\nzoneight234\n7pqrstsixteen\n",
       "281");
  test("threight\n", "88");
  test("dbfnnmcpone1sslqtnsv2ninemttk\n", "19");
  test("2nine\n", "29");
  test("eight9fhstbssrplmdlncmmqqnklb39ninejz\n", "89");
  test("ndqhpmjsxjgvdgv2six1qbnc\nsixeightsvcgjzfmqthree98\nthreeglsevenone2\n9"
       "onetwovlmzvndpts3\nsix6xjfnrqbmxs7twofhcsgnm1\n42pspfbrfive\nthreeccbdt"
       "srfv4drmvqcbdsix7sevenfiven\n",
       "355");
  return 0;
}
